import service from "./_baseServices";
class accountService {
  login = async (params) => {
    return await service.post("/account/Login", params);
  };

  register = async (params) => {
    return await service.post("/account/Register", params);
  };
}

export default new accountService();
