import axios from "axios";
//import { baseURL } from "../setting";

const axiosClient = axios.create();
// Add a request interceptor
axiosClient.interceptors.request.use(
  function (config) {
    config.baseURL = "http://149.28.129.166:3002";
    config.headers = {
      "Content-Type": "application/json",
    };
    const user = JSON.parse(localStorage.getItem("user"));

    if (user && user["token"]) {
      config.headers = {
        ...config.headers,
        "auth-token": user["token"],
      };
    }

    if (config.params) {
      //delete param is null
      for (const key of Object.keys(config.params)) {
        if (config.params[key] === "") {
          delete config.params[key];
        }
      }
    }

    // Do something before request is sent
    return config;
  },

  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
axiosClient.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    if (response && response.data) {
      return response.data;
    }
    return response;
  },
  function (error) {
    if (error && error.response && error.response.data) {
      return Promise.reject(error.response.data);
    } else if (error && error.response) {
      return Promise.reject(error.response);
    }
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);

export default axiosClient;
