import React from "react";
import { useSelector } from "react-redux";

import { CSidebar, CSidebarBrand, CSidebarNav } from "@coreui/react";

import { AppSidebarNav } from "./AppSidebarNav";

import SimpleBar from "simplebar-react";
import "simplebar/dist/simplebar.min.css";

// sidebar nav config
import navigation from "../_nav";

const AppSidebar = () => {
  const sidebarShow = useSelector((state) => state.sidebarShow);
  return (
    <CSidebar position="fixed" visible={sidebarShow}>
      <CSidebarBrand className="d-none d-md-flex" to="/">
        QUẢN LÝ BÃI XE
      </CSidebarBrand>
      <CSidebarNav>
        <SimpleBar>
          <AppSidebarNav items={navigation} />
        </SimpleBar>
      </CSidebarNav>
    </CSidebar>
  );
};

export default React.memo(AppSidebar);
