import React from "react";
import CIcon from "@coreui/icons-react";
import { cilPuzzle } from "@coreui/icons";
import { CNavGroup, CNavItem } from "@coreui/react";

const _nav = [
  {
    component: CNavGroup,
    name: "Quản Lý",
    to: "/quanly/themdancu",
    icon: <CIcon icon={cilPuzzle} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: "Thêm dân cư",
        to: "/quanly/themdancu",
      },
    ],
  },
];

export default _nav;
