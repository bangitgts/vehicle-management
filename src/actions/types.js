import accountService from "src/services/AccountService";
// const
export const LOG_IN = "LOG_IN";
export const LOG_IN_FAILED = "LOG_IN_FAILED";
export const SET = "SET";

const loginSuccessfully = (response) => ({ type: LOG_IN, payload: response });
const loginFailure = (response) => ({ type: LOG_IN_FAILED, payload: response });

const login = (params) => async (dispatch) => {
  try {
    const { success, data } = await accountService.login(params);
    if (success) {
      dispatch(loginSuccessfully(data));
      localStorage.setItem("user", JSON.stringify(data));
    }
  } catch (error) {
    dispatch(loginFailure(error.message));
  }
};

export { login };
