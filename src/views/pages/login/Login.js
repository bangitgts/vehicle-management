import React, { useRef } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { cilLockLocked, cilUser } from "@coreui/icons";
import { Toast } from "primereact/toast";
import { useForm } from "react-hook-form";

import accountService from "src/services/AccountService";

const Login = () => {
  const {
    register,
    handleSubmit,
    //formState: { errors },
  } = useForm();
  const toast = useRef(null);

  const showError = () => {
    toast.current.show({
      severity: "error",
      summary: "Đăng nhập không thành công",
      detail: "Vui lòng kiểm tra tài khoản hoặc mật khẩu",
      life: 3000,
    });
  };

  const onSubmit = async (params) => {
    accountService
      .login(params)
      .then((data) => {
        localStorage.setItem("user", JSON.stringify(data));
        window.location.replace(window.location.origin + "/#/base/accordion");
      })
      .catch((error) => {
        showError();
      });
  };
  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <Toast ref={toast} />
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={8}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm onSubmit={handleSubmit(onSubmit)}>
                    <h1>Đăng nhập</h1>
                    <p className="text-medium-emphasis">Đăng nhập với tài khoản của bạn</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser} />
                      </CInputGroupText>
                      <CFormInput
                        placeholder="Nhập tài khoản"
                        autoComplete="username"
                        {...register("username")}
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupText>
                        <CIcon icon={cilLockLocked} />
                      </CInputGroupText>
                      <CFormInput
                        type="password"
                        placeholder="Nhập mật khẩu"
                        autoComplete="current-password"
                        {...register("password")}
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs={6}>
                        <CButton type="submit" color="primary" className="px-4">
                          Login
                        </CButton>
                      </CCol>
                      <CCol xs={6} className="text-right">
                        <CButton color="link" className="px-0">
                          Forgot password?
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
