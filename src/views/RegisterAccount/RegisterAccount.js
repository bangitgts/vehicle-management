import React, { useState, useRef } from "react";
import { useForm } from "react-hook-form";
import { CCard, CCardBody, CCardHeader, CCol, CRow } from "@coreui/react";
import { Dropdown } from "primereact/dropdown";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { Toast } from "primereact/toast";
// api
import accountService from "src/services/AccountService";
const RegisterAccount = () => {
  const toast = useRef(null);
  const [genderItem, setGenderItem] = useState(null);
  const genders = [
    { name: "Nam", id: 1 },
    { name: "Nữ", id: 0 },
  ];

  const showSuccess = () => {
    toast.current.show({
      severity: "success",
      summary: "Thành công",
      detail: "Thêm dân cư thành công",
      life: 3000,
    });
  };
  const { register, handleSubmit } = useForm();

  const onSubmit = (data) => {
    let pramsObj = { ...data };
    pramsObj.gender = genderItem.id;
    accountService
      .register(pramsObj)
      .then((data) => {
        showSuccess();
        setGenderItem(null);
      })
      .catch((error) => {
        toast.current.show({
          severity: "error",
          summary: "Thất bại",
          detail: error.message,
          life: 3000,
        });
      });
  };
  const handleChangeGender = (e) => {
    setGenderItem(e.value);
  };

  return (
    <CRow>
      <CCol xs={12}>
        <Toast ref={toast} />
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Thêm dân cư</strong>
          </CCardHeader>
          <CCardBody>
            <div className="p-fluid">
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="p-field">
                  <label htmlFor="fullName">Họ và tên</label>
                  <InputText id="fullName" required type="text" {...register("fullName")} />
                </div>
                <div className="p-field">
                  <label htmlFor="username">Tài khoản khởi tạo</label>
                  <InputText id="username" required type="text" {...register("username")} />
                </div>
                <div className="p-field">
                  <label htmlFor="username">Mật khẩu khởi tạo</label>
                  <InputText id="password" required type="password" {...register("password")} />
                </div>
                <div className="p-field">
                  <label htmlFor="username">Giới Tính</label>
                  <Dropdown
                    value={genderItem}
                    options={genders}
                    onChange={handleChangeGender}
                    optionLabel="name"
                    required
                    placeholder="Chọn giới tính"
                  />
                </div>
                <Button type="submit" label="Thêm dân cư" disabled={!genderItem} />
              </form>
            </div>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  );
};

export default RegisterAccount;
