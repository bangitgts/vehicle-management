import React, { Component } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import { userRoutes, authRoutes } from "./routes/routes";
import Authmiddleware from "./routes/middleware/Authmiddleware";
import "./scss/style.scss";
import PrimeReact from "primereact/api";

PrimeReact.ripple = true;
const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

class App extends Component {
  render() {
    const NonAuthmiddleware = ({ exact = false, path, title, component: Component }) => (
      <Route exact={exact} title={title} path={path} render={(props) => <Component {...props} />} />
    );
    return (
      <HashRouter>
        <React.Suspense fallback={loading}>
          <Switch>
            {authRoutes.map((route, idx) => (
              <NonAuthmiddleware
                // eslint-disable-next-line react/no-array-index-key
                key={idx}
                path={route.path}
                exact={route.exact}
                title={route.title}
                component={route.component}
              />
            ))}
            {/* <Route path="/" name="Home" render={(props) => <DefaultLayout {...props} />} /> */}
            {userRoutes.map((route, idx) => (
              <Authmiddleware
                // eslint-disable-next-line react/no-array-index-key
                key={idx}
                path={route.path}
                exact={route.exact}
                title={route.name}
                //layout={DefaultLayout}
                component={route.component}
              />
            ))}
          </Switch>
        </React.Suspense>
      </HashRouter>
    );
  }
}

export default App;
