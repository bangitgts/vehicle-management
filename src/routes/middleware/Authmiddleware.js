import React from "react";
import PropTypes from "prop-types";
import { Route, Redirect, withRouter } from "react-router-dom";
const DefaultLayout = React.lazy(() => import("src/layout/DefaultLayout"));

const Authmiddleware = ({
  exact = false,
  path,
  title: Title, //layout: Layout, //component: Component,
}) => (
  <Route
    exact={exact}
    path={path}
    render={(props) => {
      const user = JSON.parse(localStorage.getItem("user"));
      if (!user)
        return (
          <Redirect
            //to={{ pathname: "/login", state: { from: props.location } }}
            to={{ pathname: "/login" }}
          />
        );
      else {
        return <DefaultLayout {...props} />;
      }
    }}
  />
);

export default withRouter(Authmiddleware);

Authmiddleware.propTypes = {
  exact: PropTypes.bool,
  path: PropTypes.string,
  title: PropTypes.string,
};
