import React from "react";
import { Redirect } from "react-router-dom";

// Base
const RegisterAccount = React.lazy(() => import("../views/RegisterAccount/RegisterAccount"));
const Cards = React.lazy(() => import("../views/base/cards/Cards.js"));
const Page404 = React.lazy(() => import("../views/pages/page404/Page404"));
const Login = React.lazy(() => import("../views/pages/login/Login"));
const userRoutes = [
  { path: "/", exact: true, title: "Home" },
  { path: "/quanly", title: "Quản Lý", component: Cards, exact: true },
  { path: "/quanly/themdancu", title: "Thêm dân cư", component: RegisterAccount },
  { path: "/quanly/themxe", title: "Thêm xe vào bãi", component: RegisterAccount },
  { path: "/not-found", exact: true, title: "Not Found", component: Page404 },
  {
    path: "*",
    title: "Not Found",
    component: () => {
      return <Redirect to="/not-found" />;
    },
  },
];

const authRoutes = [{ path: "/login", component: Login, title: "Login" }];

const routes = [...authRoutes, ...userRoutes];

export { userRoutes, authRoutes, routes };
