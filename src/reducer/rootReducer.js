import { LOG_IN, LOG_IN_FAILED, SET } from "../actions/types";

const initialState = {
  userLogin: [],
  sidebarShow: true,
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOG_IN: {
      return { ...state, userLogin: action.payload };
    }
    case LOG_IN_FAILED: {
      return { ...state, userLogin: action.payload };
    }
    case SET: {
      return { ...state, sidebarShow: action.sidebarShow };
    }
    default: {
      return { ...state };
    }
  }
};

export default rootReducer;
